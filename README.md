Quick Start
===========

   - go to lxplus and run

     ```
     echo '{"bob": 1}' | singularity run  docker://gitlab-registry.cern.ch/dguest/yaml2json/yaml2json -r > bob.yml
     ```

The `-r` argument runs this in reverse, so you can do things like

```
alias yaml2json='singularity run docker://gitlab-registry.cern.ch/dguest/yaml2json/yaml2json'
cat some-file.yml | yaml2json | yaml2json -r > same-as-some-file.yml
```


Thanks
======

Thanks to Matthew Feickert for his [example images][1], most things
are copied from there.

[1]: https://gitlab.cern.ch/aml/containers/docker
