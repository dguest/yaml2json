FROM python:3.7-alpine3.9

RUN \
  pip3 install --upgrade pip && \
  pip3 install pyyaml && \
  true

COPY yaml2json.py /bin/

ENTRYPOINT ["/bin/yaml2json.py"]
