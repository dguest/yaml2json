#!/usr/bin/env python3

"""
Convert a yaml file to json, or json to yaml
"""

import yaml, json
import sys
import argparse

def yaml2json(istream, ostream):
    ostream.write(
        json.dumps(
            yaml.load(istream,Loader=yaml.FullLoader),
            indent=2)
    )

def json2yaml(istream, ostream):
    ostream.write(
        yaml.dump(
            json.load(istream)
        )
    )

def get_args():
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('-r','--reverse', action='store_true')
    return parser.parse_args()

def run():
    args = get_args()
    if args.reverse:
        json2yaml(sys.stdin, sys.stdout)
    else:
        yaml2json(sys.stdin, sys.stdout)

if __name__ == '__main__':
    run()
